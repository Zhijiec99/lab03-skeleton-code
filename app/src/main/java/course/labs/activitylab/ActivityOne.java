package course.labs.activitylab;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class ActivityOne extends Activity {

    // string for logcat documentation
    private final static String TAG = "Lab-ActivityOne";

    // lifecycle counts
    private int createCount = 0;
    private int startCount = 0;
    private int resumeCount = 0;
    private int pauseCount = 0;
    private int restartCount = 0;
    private int stopCount = 0;
    private int destroyCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);

        SharedPreferences sp = getPreferences(MODE_PRIVATE);
        createCount = sp.getInt("createCount", 0);
        startCount = sp.getInt("startCount", 0);
        resumeCount = sp.getInt("resumeCount", 0);
        pauseCount = sp.getInt("pauseCount", 0);
        restartCount = sp.getInt("restartCount", 0);
        stopCount = sp.getInt("stopCount", 0);
        destroyCount = sp.getInt("destroyCount", 0);
        //Log cat print out
        Log.i(TAG, "onCreate called");

        createCount++;
        ((TextView) (findViewById(R.id.create))).setText(getText(R.string.onCreate) + " " + createCount);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_one, menu);
        return true;
    }

    // lifecycle callback overrides

    @Override
    public void onStart() {
        super.onStart();

        //Log cat print out
        Log.i(TAG, "onStart called");
        startCount++;
        ((TextView) (findViewById(R.id.start))).setText(getText(R.string.onStart) + " " + startCount);
    }


    @Override
    public void onResume() {
        super.onResume();

        //Log cat print out
        Log.i(TAG, "onResume called");
        resumeCount++;
        ((TextView) (findViewById(R.id.resume))).setText(getText(R.string.onResume) + " " + resumeCount);
        ((TextView) (findViewById(R.id.pause))).setText(getText(R.string.onPause) + " " + pauseCount);
        ((TextView) (findViewById(R.id.stop))).setText(getText(R.string.onStop) + " " + stopCount);
        ((TextView) (findViewById(R.id.restart))).setText(getText(R.string.onRestart) + " " + restartCount);
        ((TextView) (findViewById(R.id.destroy))).setText(getText(R.string.onDestroy) + " " + destroyCount);
    }

    @Override
    public void onPause() {
        super.onPause();

        //Log cat print out
        Log.i(TAG, "onPause called");
        pauseCount++;

    }

    @Override
    public void onStop() {
        super.onStop();

        //Log cat print out
        Log.i(TAG, "onStop called");
        stopCount++;

    }

    @Override
    public void onRestart() {
        super.onRestart();

        //Log cat print out
        Log.i(TAG, "onRestart called");
        restartCount++;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //Log cat print out
        Log.i(TAG, "onDestroy called");
        destroyCount++;
        SharedPreferences sp = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("createCount", createCount);
        editor.putInt("startCount", startCount);
        editor.putInt("resumeCount", resumeCount);
        editor.putInt("pauseCount", pauseCount);
        editor.putInt("restartCount", restartCount);
        editor.putInt("stopCount", stopCount);
        editor.putInt("destroyCount", destroyCount);
        editor.commit();
    }


    public void launchActivityTwo(View view) {
        startActivity(new Intent(this, ActivityTwo.class));
    }


}
